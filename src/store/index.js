import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);

const store = new Vuex.Store({
  state:{
    fromIndex: "false",
    code: ''
  },
  mutations: {
    setFromIndex(state,val){
      state.fromIndex = val;
    },
    setCode(state,val){
      state.code = val;
    }
  }
});

export default store;
