import Vue from 'vue'
import Router from 'vue-router'
import home from '@/views/home'
import recive from '@/views/recive'
import share from '@/views/share'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: home
    },
    {
      path: '/recive',
      name: 'recive',
      component: recive
    },
    {
      path: '/share',
      name: 'share',
      component: share
    }
  ]
})
