// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
import './fonts/font.css'
import { Spinner } from 'mint-ui';
import rule from './components/rule'
import giveVoucher from './components/giveVoucher'
import model from './components/model'
import store from './store/index'
import { Toast } from 'mint-ui';

Vue.component(Spinner.name, Spinner);
Vue.component('rule', rule);
Vue.component('giveVoucher', giveVoucher);
Vue.component('model', model);
import { Lazyload } from 'mint-ui';

Vue.use(Lazyload);

Vue.use(MintUI);

import axios from 'axios'
// 添加请求拦截器
axios.interceptors.request.use(function (config) {
  // 在发送请求之前做些什么
  if (!window.navigator.onLine) {
    Toast({
      message: '网络请求失败，请检查您的网络设置',
      position: 'middle',
      duration: 2000
    })
    let err = {
      offLine: true
    }
  }
  return config;
}, function (error) {
  // 对请求错误做些什么
  return Promise.reject(error);
});

if (process.env.NODE_ENV !== 'production') {
  axios.defaults.baseURL = '/api/operation/'
} else {
  axios.defaults.baseURL = '/operation/'
}

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
